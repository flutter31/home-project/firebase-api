import * as functions from 'firebase-functions';

import { getCompanies } from './routes/get-companies';
import { getCompany } from './routes/get-company';
import { createCompanyReview } from './routes/reviews/create-review';
import { removeCompanyReview } from './routes/reviews/remove-review';
import { updateCompanyReview } from './routes/reviews/update-review';

export const companies = functions.https.onRequest(
  async (request, response) => {
    switch (request.method) {
      case 'GET':
        await getCompanies(request, response);
        break;
      default:
        response
          .status(404)
          .send(
            `Route "companies" is not set for ${request.method} HTTP request method!`
          );
    }
  }
);

export const company = functions.https.onRequest(async (request, response) => {
  switch (request.method) {
    case 'GET':
      await getCompany(request, response);
      break;
    default:
      response
        .status(404)
        .send(
          `Route "company" is not set for ${request.method} HTTP request method!`
        );
  }
});

export const companyReview = functions.https.onRequest(
  async (request, response) => {
    switch (request.method) {
      case 'POST':
        await createCompanyReview(request, response);
        break;
      case 'DELETE':
        await removeCompanyReview(request, response);
        break;
      case 'PATCH':
        await updateCompanyReview(request, response);
        break;
      default:
        response
          .status(404)
          .send(
            `Route "company" is not set for ${request.method} HTTP request method!`
          );
    }
  }
);
