import * as admin from 'firebase-admin';

import {
  CompanyScheduleType,
  WeekDayEnum,
  ICompanyReview,
  IProfile,
} from '../../../core/models';

/* COLLECTIONS GETTERS */

export const companiesCollection: admin.firestore.CollectionReference<any> = admin
  .firestore()
  .collection('Company');

export const getProfilesCollection = (): admin.firestore.CollectionReference<
  any
> => admin.firestore().collection('Profiles');

export const getCompanyScheduleCollection = (
  id: string
): admin.firestore.CollectionReference<any> =>
  companiesCollection.doc(id).collection('Schedule');

export const getCompanyReviewsCollection = (
  id: string
): admin.firestore.CollectionReference<any> =>
  companiesCollection.doc(id).collection('Review');

/* VALIDATORS */

export const validateCompanySchedule = (
  schedule: Partial<CompanyScheduleType>
): CompanyScheduleType => {
  const defaultData: [number, number, number, number] = [10, 0, 23, 59];

  if (!schedule) {
    // tslint:disable-next-line: no-parameter-reassignment
    schedule = {};
  }

  for (const key of Object.values(WeekDayEnum)) {
    if (!schedule[key] || schedule[key]?.length !== 4) {
      schedule[key] = defaultData;
    }
  }

  return schedule as CompanyScheduleType;
};

const validateReviewRate = (rate: number) => {
  if (rate < 1) {
    return 1;
  }

  if (rate > 5) {
    return 5;
  }

  return Math.round(rate);
};

/* HELPERS */

export const getCompanyReview = (
  reviewSnapshot: admin.firestore.QueryDocumentSnapshot<ICompanyReview>,
  profileSnapshot: admin.firestore.QueryDocumentSnapshot<IProfile>
): ICompanyReview => {
  const review: ICompanyReview = reviewSnapshot.data();
  const user: Partial<IProfile> = getUserForCompanyReview(
    profileSnapshot.data()
  );

  return {
    ...review,
    id: reviewSnapshot.id,
    rate: validateReviewRate(review.rate),
    user,
  };
};

export const getUserAvatarUrl = (id: string): string => `profileAvatar/${id}`;

export const getProfileId = async (userId: string): Promise<string | null> => {
  const profiles: admin.firestore.QuerySnapshot<IProfile> = await getProfilesCollection().get();

  let profileId: string | null = null;

  profiles.forEach((p) => {
    if (p.data().userId === userId) {
      profileId = p.id;
    }
  });

  return profileId;
};

export const getUserForCompanyReview = (data: IProfile): Partial<IProfile> => {
  const userResponse: IProfile = data;

  return {
    avatarUrl: getUserAvatarUrl(userResponse.userId),
    userId: userResponse.userId,
    firstname: userResponse.firstname,
    lastname: userResponse.lastname,
  };
};
