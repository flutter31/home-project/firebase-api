import * as admin from 'firebase-admin';
import * as express from 'express';

import { ICompany, IRequest, CompanyScheduleType } from '../../../core/models';
import {
  companiesCollection,
  getCompanyScheduleCollection,
  validateCompanySchedule,
} from '../helpers';

export const getCompanies = async (
  request: IRequest,
  response: express.Response
) => {
  const companiesSnapshot: admin.firestore.QuerySnapshot<ICompany> = await companiesCollection.get();

  const companies: ICompany[] = [];

  companiesSnapshot.forEach((companySnapshot) => {
    companies.push({
      ...companySnapshot.data(),
      id: companySnapshot.id,
      logoUrl: `companyLabels/${companySnapshot.id}`,
    });
  });

  const schedulesSnapshots$: Promise<
    admin.firestore.QuerySnapshot<CompanyScheduleType>
  >[] = companies
    .map(({ id }) => getCompanyScheduleCollection(id))
    .map((collection: admin.firestore.CollectionReference<any>) =>
      collection.get()
    );

  const schedulesSnapshots: admin.firestore.QuerySnapshot<
    CompanyScheduleType
  >[] = await Promise.all(schedulesSnapshots$);

  schedulesSnapshots.forEach((schedulesSnapshot, index) =>
    schedulesSnapshot.forEach(
      (scheduleSnapshot) =>
        (companies[index].schedule = validateCompanySchedule(
          scheduleSnapshot.data()
        ))
    )
  );

  response.send({ data: companies });
};
