import * as admin from 'firebase-admin';
import * as express from 'express';

import {
  ICompany,
  IRequest,
  CompanyScheduleType,
  ICompanyReview,
  IProfile,
} from '../../../core/models';
import {
  companiesCollection,
  getCompanyScheduleCollection,
  validateCompanySchedule,
  getCompanyReviewsCollection,
  getCompanyReview,
} from '../helpers';

export const getCompany = async (
  request: IRequest,
  response: express.Response
) => {
  const id: string = String(request.query['id']);
  const companySnapshot: admin.firestore.DocumentReference<ICompany> = await companiesCollection.doc(
    id
  );

  const company: ICompany | undefined = (await companySnapshot.get()).data();

  if (!company) {
    response.status(404).send(`Company with #id${id} not found!`);
  }

  const schedulesSnapshot: admin.firestore.QuerySnapshot<CompanyScheduleType> = await getCompanyScheduleCollection(
    id
  ).get();

  schedulesSnapshot.forEach((scheduleSnapshot) => {
    if (company) {
      company.schedule = validateCompanySchedule(scheduleSnapshot.data());
    }
  });

  const reviewsSnapshot: admin.firestore.QuerySnapshot<ICompanyReview> = await getCompanyReviewsCollection(
    id
  ).get();

  const reviewSnapshots: admin.firestore.QueryDocumentSnapshot<
    ICompanyReview
  >[] = [];

  reviewsSnapshot.forEach((reviewSnapshot) => {
    reviewSnapshots.push(reviewSnapshot);
  });

  const profileSnapshots: admin.firestore.QueryDocumentSnapshot<
    IProfile
  >[] = await Promise.all(
    reviewSnapshots.map((snapshot) => snapshot.get('user').get())
  );

  reviewSnapshots.forEach((reviewSnapshot, index) => {
    if (company) {
      if (!company.reviews) {
        company.reviews = [];
      }

      reviewSnapshots.push(reviewSnapshot);

      company.reviews.push(
        getCompanyReview(reviewSnapshot, profileSnapshots[index])
      );
    }
  });

  response.send({
    data: {
      ...company,
      id,
      logoUrl: `companyLabels/${companySnapshot.id}`,
    },
  });
};
