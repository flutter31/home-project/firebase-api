import * as admin from 'firebase-admin';
import * as express from 'express';

import {
  IRequest,
  RequestError,
  ICreateCompanyReviewRequest,
  ICreateCompanyReviewDto,
  ICompanyReview,
} from '../../../../core/models';
import { authorize } from '../../../../core/helpers/auth';
import {
  getProfilesCollection,
  getProfileId,
  getUserForCompanyReview,
  getCompanyReviewsCollection,
} from '../../helpers';

export const createCompanyReview = async (
  request: IRequest,
  response: express.Response
) => {
  let userId: string | undefined;

  try {
    userId = await authorize(request);
  } catch (error) {
    error instanceof RequestError
      ? response.status(401).send(error.message)
      : response.status(500).send('Smth went wrong :(');
  }

  const reqBody: ICreateCompanyReviewRequest = request.body;

  const id: string = String(request.query['id']);

  if (!id) {
    response.status(400).send(`Company ID is not provided!`);
  }

  let reviewCollection: admin.firestore.CollectionReference<any> | null = null;

  try {
    reviewCollection = getCompanyReviewsCollection(id);
  } catch (error) {
    response.status(404).send(`Company with id#${id} is not found!`);
  }

  const profilesCollection: admin.firestore.CollectionReference<any> = getProfilesCollection();

  const profileId = await getProfileId(String(userId));

  if (!profileId) {
    response
      .status(403)
      .send(
        `You haven't filled in your profile, so you can't leave any reviews!`
      );
    return;
  }

  const profileSnapshot = await profilesCollection.doc(String(profileId)).get();

  const dto: ICreateCompanyReviewDto = {
    ...reqBody,
    user: profileSnapshot.ref,
  };

  let res:
    | admin.firestore.DocumentReference<ICompanyReview>
    | undefined
    | null = null;

  try {
    res = await reviewCollection?.add(dto);
  } catch (error) {
    response.status(500).send(`Smth went wrong!`);
  }

  const createdReview: ICompanyReview = {
    id: String(res?.id),
    message: dto.message,
    rate: dto.rate,
    user: getUserForCompanyReview(profileSnapshot.data()),
  };

  response.send({
    data: createdReview,
  });
};
