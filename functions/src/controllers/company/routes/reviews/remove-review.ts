import * as admin from 'firebase-admin';
import * as express from 'express';

import {
  IRequest,
  RequestError,
  IProfile,
  ICompanyReviewResponse,
} from '../../../../core/models';
import { authorize } from '../../../../core/helpers/auth';
import { getCompanyReviewsCollection } from '../../helpers';

export const removeCompanyReview = async (
  request: IRequest,
  response: express.Response
) => {
  let userId: string | undefined;

  try {
    userId = await authorize(request);
  } catch (error) {
    error instanceof RequestError
      ? response.status(401).send(error.message)
      : response.status(500).send('Smth went wrong :(');
  }

  const companyIdParam = request.query['companyId'];

  if (!companyIdParam) {
    response.status(400).send(`Company ID is not provided!`);
  }

  const companyId: string = String(companyIdParam);

  let reviewCollection: admin.firestore.CollectionReference<any> | null = null;

  try {
    reviewCollection = getCompanyReviewsCollection(companyId);
  } catch (error) {
    response.status(404).send(`Company with id#${companyId} is not found!`);
  }

  const reviewIdParams = request.query['reviewId'];

  if (!reviewIdParams) {
    response.status(400).send(`Review ID is not provided!`);
  }

  const reviewId: string = String(reviewIdParams);
  let user: IProfile | null | undefined = null;
  let reviewRef:
    | admin.firestore.DocumentReference<ICompanyReviewResponse>
    | undefined;

  try {
    reviewRef = reviewCollection?.doc(reviewId);
    const reviewSnapshot:
      | admin.firestore.DocumentSnapshot<ICompanyReviewResponse>
      | undefined = await reviewRef?.get();

    user = (await reviewSnapshot?.data()?.user.get())?.data();
  } catch (error) {
    response.status(404).send(`Review with #id${reviewId} is not found!`);
  }

  if (!user) {
    response.status(404).send(`Review with #id${reviewId} is not found!`);
  }

  if (user?.userId !== userId) {
    response
      .status(403)
      .send(
        `You can't remove reviews what you haven't created! ${user?.userId} ${userId}`
      );
    return;
  }

  try {
    await reviewRef?.delete();
  } catch (error) {
    response.status(500).send(`Smth went wrong!`);
  }

  response.send({
    success: true,
  });
};
