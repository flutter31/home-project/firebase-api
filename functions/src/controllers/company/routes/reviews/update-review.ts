import * as admin from 'firebase-admin';
import * as express from 'express';

import {
  IRequest,
  RequestError,
  IProfile,
  IUpdateCompanyReviewRequest,
  ICompanyReviewResponse,
} from '../../../../core/models';
import { authorize } from '../../../../core/helpers/auth';
import { getCompanyReviewsCollection } from '../../helpers';

export const updateCompanyReview = async (
  request: IRequest,
  response: express.Response
) => {
  let userId: string | undefined;

  try {
    userId = await authorize(request);
  } catch (error) {
    error instanceof RequestError
      ? response.status(401).send(error.message)
      : response.status(500).send('Smth went wrong :(');
  }

  const companyIdParam = request.query['companyId'];

  if (!companyIdParam) {
    response.status(400).send(`Company ID is not provided!`);
  }

  const companyId: string = String(companyIdParam);

  let reviewCollection: admin.firestore.CollectionReference<any> | null = null;

  try {
    reviewCollection = getCompanyReviewsCollection(companyId);
  } catch (error) {
    response.status(404).send(`Company with id#${companyId} is not found!`);
  }

  const reviewIdParams = request.query['reviewId'];

  if (!reviewIdParams) {
    response.status(400).send(`Review ID is not provided!`);
  }

  const reviewId: string = String(reviewIdParams);

  let reviewRef:
    | admin.firestore.DocumentReference<ICompanyReviewResponse>
    | undefined;
  let user: IProfile | null | undefined = null;

  try {
    reviewRef = reviewCollection?.doc(reviewId);
    const reviewSnapshot:
      | admin.firestore.DocumentSnapshot<ICompanyReviewResponse>
      | undefined = await reviewRef?.get();

    user = (await reviewSnapshot?.data()?.user.get())?.data();
  } catch (error) {
    response.status(404).send(`Review with #id${reviewId} is not found!`);
  }

  if (!user) {
    response.status(404).send(`Review with #id${reviewId} is not found!`);
  }

  if (user?.userId !== userId) {
    response
      .status(403)
      .send(
        `You can't update reviews what you haven't created! ${user?.userId} ${userId}`
      );
    return;
  }

  const reqBody: IUpdateCompanyReviewRequest = request.body;

  try {
    await reviewRef?.update(reqBody);
  } catch (error) {
    response.status(500).send(`Smth went wrong!`);
  }

  response.send({
    success: true,
  });
};
