import * as functions from 'firebase-functions';

import { getProfile } from './routes/get-profile';
import { getProfileAvatar } from './routes/get-profile-avatar';
import { fillInProfile } from './routes/fill-in-profile';
import { updateProfile } from './routes/update-profile';

export const profile = functions.https.onRequest(async (request, response) => {
  switch (request.method) {
    case 'GET':
      await getProfile(request, response);
      break;
    case 'POST':
      await fillInProfile(request, response);
      break;
    case 'PATCH':
      await updateProfile(request, response);
      break;
    default:
      response
        .status(404)
        .send(
          `Route "profile" is not set for ${request.method} HTTP request method!`
        );
  }
});

export const profileAvatar = functions.https.onRequest(
  async (request, response) => {
    switch (request.method) {
      case 'GET':
        await getProfileAvatar(request, response);
        break;
      default:
        response
          .status(404)
          .send(
            `Route "profileAvatar" is not set for ${request.method} HTTP request method!`
          );
    }
  }
);
