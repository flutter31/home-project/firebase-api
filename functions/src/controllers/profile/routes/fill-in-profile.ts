import * as admin from 'firebase-admin';
import * as express from 'express';

import {
  IProfile,
  RequestError,
  IRequest,
  IFillInProfileRequest,
  IFillInProfileDto,
} from '../../../core/models';
import { authorize } from '../../../core/helpers/auth';
import { getProfilesCollection, getProfileId } from '../../company/helpers';

export const fillInProfile = async (
  request: IRequest,
  response: express.Response
) => {
  let userId: string | undefined;

  try {
    userId = await authorize(request);
  } catch (error) {
    error instanceof RequestError
      ? response.status(401).send(error.message)
      : response.status(500).send('Smth went wrong :(');
  }

  userId = String(userId);

  const profilesCollection: admin.firestore.CollectionReference<any> = getProfilesCollection();

  const profileId = await getProfileId(userId);

  if (profileId) {
    response.status(403).send('Your profile is already filled in!');
    return;
  }

  const reqBody: IFillInProfileRequest = request.body;

  if (!reqBody) {
    response.status(400).send('Your did not provide profile data!');
    return;
  }

  const dto: IFillInProfileDto = { ...reqBody, userId };

  let res:
    | admin.firestore.DocumentReference<IProfile>
    | undefined
    | null = null;

  try {
    res = await profilesCollection.add(dto);
  } catch (error) {
    response.status(500).send('Smth went wrong :(');
  }

  const profile: IProfile = { userId: String(userId), ...dto };

  response.send({ id: res?.id, data: profile });
};
