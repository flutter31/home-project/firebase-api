import * as admin from 'firebase-admin';
import * as express from 'express';

import {
  RequestError,
  IRequest,
  IUpdateProfileRequest,
  IUpdateProfileDto,
} from '../../../core/models';
import { authorize } from '../../../core/helpers/auth';
import { getProfilesCollection, getProfileId } from '../../company/helpers';

export const updateProfile = async (
  request: IRequest,
  response: express.Response
) => {
  let userId: string | undefined;

  try {
    userId = await authorize(request);
  } catch (error) {
    error instanceof RequestError
      ? response.status(401).send(error.message)
      : response.status(500).send('Smth went wrong :(');
  }

  const profilesCollection: admin.firestore.CollectionReference<any> = getProfilesCollection();

  const profileId = await getProfileId(String(userId));

  if (!profileId) {
    response.status(404).send('Your profile has not filled in yet!');
    return;
  }

  const reqBody: IUpdateProfileRequest = request.body;

  if (!reqBody) {
    response.status(400).send('Your did not provide profile data!');
    return;
  }

  const dto: IUpdateProfileDto = reqBody as IUpdateProfileDto;

  try {
    await profilesCollection.doc(profileId).update(dto);
  } catch (error) {
    response.status(500).send('Smth went wrong :(');
  }

  response.send({ success: true });
};
