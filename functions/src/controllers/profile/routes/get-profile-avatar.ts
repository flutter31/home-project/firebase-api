import * as express from 'express';

import { IRequest, RequestError } from '../../../core/models';
import { authorize } from '../../../core/helpers/auth';
import { getUserAvatarUrl } from '../../company/helpers';

export const getProfileAvatar = async (
  request: IRequest,
  response: express.Response
) => {
  let userId: string | undefined;

  try {
    userId = await authorize(request);
  } catch (error) {
    error instanceof RequestError
      ? response.status(401).send(error.message)
      : response.status(500).send('Smth went wrong :(');
  }

  response.send({ data: getUserAvatarUrl(String(userId)) });
};
