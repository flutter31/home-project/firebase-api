import * as admin from 'firebase-admin';
import * as express from 'express';

import { IProfile, RequestError, IRequest } from '../../../core/models';
import { authorize } from '../../../core/helpers/auth';
import { getProfilesCollection, getProfileId } from '../../company/helpers';

export const getProfile = async (
  request: IRequest,
  response: express.Response
) => {
  let userId: string | undefined;

  try {
    userId = await authorize(request);
  } catch (error) {
    error instanceof RequestError
      ? response.status(401).send(error.message)
      : response.status(500).send('Smth went wrong :(');
  }

  const profilesCollection: admin.firestore.CollectionReference<any> = getProfilesCollection();

  const profileId = await getProfileId(String(userId));

  if (!profileId) {
    response.send({ data: null });
  }

  const profile: IProfile = (
    await profilesCollection.doc(String(profileId)).get()
  ).data();

  response.send({ id: profileId, data: profile });
};
