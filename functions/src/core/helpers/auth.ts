import * as admin from 'firebase-admin';

import { IRequest, RequestError } from '../models';

export const authorize = async (request: IRequest): Promise<string> => {
  const authHeader: string | undefined = request.headers.authorization;

  if (!authHeader || !authHeader.startsWith('Bearer ')) {
    console.log('error:authorized ');
    throw new RequestError(
      'You are not authorized! Auth token is not provided.'
    );
  }

  const token: string | undefined = authHeader?.substring(7, authHeader.length);

  if (!token) {
    throw new RequestError(
      'You are not authorized! Auth token is not provided.'
    );
  }

  const decodeToken: admin.auth.DecodedIdToken | undefined = await admin
    .auth()
    .verifyIdToken(String(token))
    .catch(() => undefined);

  if (!decodeToken) {
    throw new RequestError('You are not authorized! Auth token is invalid.');
  }

  return decodeToken?.uid;
};
