import * as express from 'express';
import * as admin from 'firebase-admin';

/* COMMON */
export interface IResponse<T> {
  readonly id?: string;
  data: T;
}

export interface IRequest extends express.Request {
  rawBody: Buffer;
}

export class RequestError extends Error {
  constructor(public readonly message: string) {
    super(message);
  }
}

/* PROFILE */

export interface IProfile {
  readonly userId: string;
  firstname: string;
  lastname: string;
  birthDay: string;
  sex: string;
  avatarUrl?: string;
}

export interface IFillInProfileRequest {
  firstname: string;
  lastname: string;
  birthDay: string;
  sex: string;
}

export interface IFillInProfileDto {
  readonly userId: string;
  firstname: string;
  lastname: string;
  birthDay: string;
  sex: string;
}

export interface IUpdateProfileRequest {
  firstname?: string;
  lastname?: string;
  birthDay?: string;
  sex?: string;
}

export interface IUpdateProfileDto {
  firstname?: string;
  lastname?: string;
  birthDay?: string;
  sex?: string;
}

/* COMPANY */
export interface ICompany {
  readonly id: string;
  title: string;
  subtitle: string;
  description: string;
  primaryColor: string;
  schedule: CompanyScheduleType;
  logoUrl: string;
  reviews?: ICompanyReview[];
}

export type CompanyScheduleType = {
  [key in WeekDayEnum]: [number, number, number, number];
};

export enum WeekDayEnum {
  MONDAY = 'monday',
  THUESDAY = 'thuesday',
  WEDNESDAY = 'wednesday',
  THURSDAY = 'thursday',
  FRIDAY = 'friday',
  SATURDAY = 'saturday',
  SUNDAY = 'sunday',
}

/* REVIEW */

export interface ICompanyReview {
  readonly id: string;
  user: Partial<IProfile>;
  rate: number;
  message: string;
}

export interface ICompanyReviewResponse {
  user: admin.firestore.DocumentReference<IProfile>;
  rate: number;
  message: string;
}

export interface ICreateCompanyReviewRequest {
  rate: number;
  message: string;
}

export interface ICreateCompanyReviewDto {
  rate: number;
  message: string;
  user: admin.firestore.DocumentReference<IProfile>;
}

export interface IUpdateCompanyReviewRequest {
  rate?: number;
  message?: string;
}
